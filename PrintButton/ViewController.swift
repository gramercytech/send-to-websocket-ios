//
//  ViewController.swift
//  PrintButton
//
//  Created by Andrey on 3/28/18.
//  Copyright © 2018 Andrey Kats. All rights reserved.
//

import UIKit
import SnapKit
import Starscream

class ViewController: UIViewController, WebSocketDelegate {
	var socket = WebSocket(url: URL(string: "ws://10.0.0.99:8080")!)
	var servers = [WebSocketClient]()
	
	let mainView: UIView = {
		let view = UIView()
		view.backgroundColor = UIColor(white: 0.6, alpha: 0.4)
		view.layer.cornerRadius = 20
		return view
	}()
	
	let subView: UIView = {
		let view = UIView()
//		view.backgroundColor = UIColor(white: 0.6, alpha: 0.4)
		view.layer.cornerRadius = 20
		return view
	}()
	
	let urlTextField: UITextField = {
		let view = UITextField()
		view.backgroundColor = .orange
		view.placeholder = "Text"
//		view.layer.cornerRadius = 20
		return view
	}()
	
	let ipTextField: UITextField = {
		let view = UITextField()
		view.backgroundColor = .red
		view.placeholder = "Server IP Address"
		
//		view.layer.cornerRadius = 20
		return view
	}()
	
	let printButton: UIButton! = {
		let view = UIButton()
		view.setTitle("Send", for: .normal)
		view.addTarget(self, action: #selector(printButtonPressed(sender:)), for: .touchDown)
		view.backgroundColor = .blue
		view.layer.cornerRadius = 15
		return view
	}()
	
	lazy var mainStackView: UIStackView = {
		let view = UIStackView(arrangedSubviews: [ipTextField, urlTextField, printButton])
		view.axis = .vertical
		view.distribution = .fillEqually
		view.spacing = 20
		return view
	}()
	
	func setupViews() {
		view.backgroundColor = .white
		title = "Send to WebSocket"
		navigationController?.navigationBar.prefersLargeTitles = true
		
		view.addSubview(mainView)
		mainView.snp.makeConstraints { (make) in
			make.topMargin.left.equalTo(20)
			make.right.bottomMargin.equalTo(-20)
		}
		
		mainView.addSubview(subView)
		subView.snp.makeConstraints { (make) in
			make.top.left.equalTo(20)
			make.right.bottom.equalTo(-20)
			make.center.equalTo(self.mainView)
		}
		
		subView.addSubview(mainStackView)
		mainStackView.snp.makeConstraints { (make) in
			make.height.equalTo(200)
			make.width.equalTo(self.subView)
			make.center.equalTo(self.subView)
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		setupViews()
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	@objc func printButtonPressed(sender: UIButton!) {
		let ipAddress = ipTextField.text
		socket = WebSocket(url: URL(string: "ws://\(ipAddress!):8080/")!)
		socket.delegate = self
		socket.connect()
	}
	
	func websocketDidConnect(socket: WebSocketClient) {
		print("websocket is connected")
		socket.write(string: urlTextField.text!)
		socket.disconnect()
	}
	
	func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
		if let message = error {
			print("websocket is disconnected: \(message.localizedDescription)")
		}
	}
	
	func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
		print("websocket received text: \(text)")
		let alert = UIAlertController(title: "Success", message: "Server recieved print request", preferredStyle: .alert)
		let okAction = UIAlertAction(title: "OK", style: .default) { action in
			// Ok Pressed
		}
		alert.addAction(okAction)
		present(alert, animated: true, completion: nil)
	}
	
	func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
		print("websocket received data: \(data.count)")
	}
}

